# clk_dump to .dot converting utility

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)

## About <a name = "about"></a>

<p>This utility converts the clk_dump file into a .dot file for subsequent graphical representation by Graphviz tools.</p>


## Getting Started <a name = "getting_started"></a>

### Install git and python3

If you're on Linux, you probably already have these.

### Install Graphviz

On Debian and Ubuntu like distributions you can use the command:
```
$ sudo apt install graphviz
```

## Usage <a name = "usage"></a>

<p>Main utility file - json_to_dot_tree.py. </p>
<p>Utility reads standard input and outputs to standard output.</p>
<p>To use the utility, use a command like:</p>

<p>To get .dot file:</p>

```
cat input_file | python3 json_to_dot_tree.py > output_file.dot
```
<p>To get .svg file:</p>

```
$ cat input_file | python3 json_to_dot_tree.py | dot -Tsvg -o output_file.svg
```

<p>By default highlighting of entities with the parameter "enable_count" >= 1 is enabled.</p>

<p>To get a colorless graph, you can use the parameters "--nocolor" or "-n":</p>

```
$ cat input_file | python3 json_to_dot_tree.py -n | dot -Tsvg -o output_file.svg
```
