#LR(0) parser
#S: {A}
#A: B}
#A: A,B}
#B: B,A
#B: d
#d - terminal character

#d is data of type "fw-clk-m2mc": { "enable_count": 0,"prepare_count": 0,"protect_count": 0,"rate": 0,"min_rate": 0,"max_rate": 600000000,"accuracy": 0,"phase": 0,"duty_cycle": 50000}

#control table:
#   | A  B  | d  ,  {  }  $
#0  |       |       s1
#1  | 7  2  | s4
#2  |       | s4 s5    s3
#3  |       | r1 r1 r1 r1 r1
#4  |       | r4 r4 r4 r4 r4
#5  | 6  2  | s4
#6  |       | r3 r3 r3 r3 r3
#7  |       |    s9    s8
#8  |       | r0 r0 r0 r0 r0
#9  |   10  | s4
#10 |   11  | s4 s5    s12
#11 |       |          s12   - not used
#12 |       | r2 r2 r2 r2 r2

import re
import string
from unittest import case


def err_msg():
    global state
    print("error state")
    print(state)

def dbg_msg():
    print("##################")
    print(stack)
    print(tape[i:-1])
    print("##################")

def shift(st):
    global i
    global stack
    global tape
    global state
    stack.append(tape[i])
    state = st
    stack.append(state)
    i = i + 1
    dbg_msg()

json_string = input()
#print(json_string)
pattern = '\"[a-z0-9_\-\.]+\": \{ \"enable_count\": [0-9]+,\"prepare_count\": [0-9]+,\"protect_count\": [0-9]+,\"rate\": [0-9]+,\"min_rate\": [0-9]+,\"max_rate\": [0-9]+,\"accuracy\": [0-9]+,\"phase\": [0-9]+,\"duty_cycle\": [0-9]+'
pattern2 = '\"[a-z0-9_-]+\"'
repl = 'd'
#print(re.sub(pattern, repl, json_string))
prepare_json_string = re.sub(pattern, repl, json_string)
#print(prepare_json_string)
stack = []
stack.append('&')
prepare_json_string = prepare_json_string + '&'

#LR(0) FSM
tape = prepare_json_string
#print(prepare_json_string)
i = 0
state = 0



while (tape[i] != '&' or state != -1):
    if state == 0:
        if tape[i] == '{':
            shift(1)
        else:
            err_msg()
            break
    elif state == 1:
        if tape[i] == 'd':
            shift(4)
        else:
            err_msg()
            break
    elif state == 2:
        if tape[i] == 'd':
            shift(4)
            dbg_msg()
        elif tape[i] == ',':
            shift(5)
            dbg_msg()
        elif tape[i] == '}':
            shift(3)
        else:
            err_msg()
            break
    elif state == 3:
        if stack[-2] == '}' and stack[-4] == 'B':
            stack.pop()
            stack.pop()
            stack.pop()
            stack.pop()
            state = stack[-1]
            stack.append("A")
            if(state == 1):
                state = 7
            elif state == 5:
                state = 6
            else:
                err_msg()
                break
            stack.append(state)
            dbg_msg()
        else:
            err_msg()
            break
    elif state == 4:
        if stack[-2] == 'd':
            stack.pop()
            stack.pop()
            state = stack[-1]
            stack.append("B")
            if(state == 1):
                state = 2
            elif state == 5:
                state = 2
            elif state == 9:
                state = 10
            elif state == 10:
                state = 11
            else:
                err_msg()
                break
            stack.append(state)
            dbg_msg()
        else:
            err_msg()
            break
    elif state == 5:#################################################
        if tape[i] == 'd':
            shift(4)
    elif state == 6:
        if stack[-2] == 'A' and stack[-4] == ',' and stack[-6] == 'B':
            for j in range(6):
                stack.pop()
            state = stack[-1]
            stack.append("B")
            if(state == 1):
                state = 2
            elif state == 5:
                state = 2
            elif state == 9:
                state = 10
            elif state == 10:
                state = 11
            else:
                err_msg()
                break
            stack.append(state)
            dbg_msg()
        else:
            err_msg()
            break
    elif state == 7:
        if tape[i] == '}':
            shift(8)
        elif tape[i] == ',':
            shift(9)
        else:
            err_msg()
            break
    elif state == 8:
        if len(stack) == 7:
            for j in range(6):
                stack.pop()
            stack.append("S")
            state = -1
            dbg_msg()
        else:
            err_msg()
            break
    elif state == 9:
        if tape[i] == 'd':
            shift(4)
        else:
            err_msg()
            break
    elif state == 10:####################################S
        if tape[i] == 'd':
            shift(4)
        elif tape[i] == '}':
            shift(12)
        elif tape[i] == ',':
            shift(5)
        else:
            err_msg()
            break
    elif state == 11:
        if tape[i] == '}':
            shift(12)
        else:
            err_msg()
            break
    elif state == 12:
        if stack[-2] == '}' and stack[-4] == 'B' and stack[-6] == ',' and stack[-8] == 'A':
            for j in range(8):
                stack.pop()
            state = stack[-1]
            stack.append("A")
            if(state == 1):
                state = 7
            elif state == 5:
                state = 6
            else:
                err_msg()
                break
            stack.append(state)
            dbg_msg()
        else:
            err_msg()
            break
    else:
        err_msg()
        break

